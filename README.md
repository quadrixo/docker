# docker

A collection of docker images.

## [PHPUnit](./phpunit)

Test your PHP code against differents versions of PHP and PHPUnit.

# License

These images are licensed under the [CeCILL](https://cecill.info/) license.

[CeCILL 2.1](./LICENSE)
