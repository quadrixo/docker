#!/bin/sh

PHP_VERSION=
PHPUNIT_VERSION=
XDEBUG_VERSION=

usage() {
    echo "Usage: $0 -p [PHP_VERSION] -u [PHPUNIT_VERSION] -x [XDEBUG_VERSION]"
    exit 1
}

while getopts p:u: arg
do
    case "${arg}" in
        p) PHP_VERSION=${OPTARG};;
        u) PHPUNIT_VERSION=${OPTARG};;
        x) XDEBUG_VERSION=${OPTARG};;
    esac
done

if [ -z "$PHP_VERSION" ] || [ -z "$PHPUNIT_VERSION" ] || [ -z "$XDEBUG_VERSION" ]; then
    usage
fi

TMP_NAME=$(mktemp)
wget -q https://phar.phpunit.de/phpunit-${PHPUNIT_VERSION}.phar -O $TMP_NAME
chmod 755 $TMP_NAME

VERSION="$($TMP_NAME --version | sed -r 's/[^0-9]+([0-9]+(\.[0-9]+)*).+/\1/')-$PHP_VERSION"
docker build \
    --build-arg PHP_VERSION=$PHP_VERSION \
    --build-arg PHPUNIT_VERSION=$PHPUNIT_VERSION \
    --build-arg XDEBUG_VERSION=$PHPUNIT_VERSION \
    --build-arg XDEBUG_MAJOR=${XDEBUG_VERSION%%.*} \
    -t quadrixo/phpunit:$VERSION .
