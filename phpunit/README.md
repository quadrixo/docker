# Hosting

The `Dockerfile` of theses images is hosted in gitlab.

 * [Dockerfile](./Dockerfile)

# Supported tags

Tags are named against the version of PHPUnit and PHP : `phpunit:<php-version>-<phpunit-version>`

 * 9-7.4, latest : use PHPUnit 9 with PHP 7.4
 * 9-7.3 : use PHPUnit 9 with PHP 7.3
 * 8-7.2 : use PHPUnit 8 with PHP 7.2

# Build your own image

The `Dockerfile` is variabilized to be easily build with the PHPUnit version and PHP version of your choice.

Variables :
 * PHP_VERSION: used to resolve the base image (php:[PHP_VERSION]-alpine)
 * PHPUNIT_VERSION: the version of PHPUnit to install

```sh
$ docker build \
    --build-arg PHP_VERSION=7.4 \
    --build-arg PHPUNIT_VERSION=9 \
    --tag quadrixo/phpunit:9-7.4 \
    .
```

> The base image is alpine php image provided by Docker
>
> ex: [php:7.4-alpine](https://hub.docker.com/layers/php/library/php/7.4-alpine/images/sha256-182f330dc325474155433cdf326adb6a991b9b16147731934a3f3d4a05743a50?context=explore)

> Verify the compatibility between versions of PHP and PHPUnit

> The PHP extension XDebug is enabled with the version 2.9.8

# How to use this image

## Create a `Dockerfile` in your project

```
FROM quadrixo/phpunit:9-7.4
COPY ./ /data
CMD [ "phpunit", "-c", "phpunit.xml" ]
```

Then, run the commands to build and run the Docker image :

```sh
$ docker build -t my-phpunit .
$ docker run -it my-phpunit
```

## Without `Dockerfile`

If you don't want to include a Dockerfile in your project, you just have to run :

```sh
$ docker run -it -v "$PWD":/data \
    quadrixo/phpunit:9-7.4 phpunit -c phpunit.xml
```

## In the `composer.json` file

If you use [composer](https://getcomposer.org/), it is possible to integrate multiple version of PHPUnit test.

Edit the `composer.json` file with :

```json
{
    "name": "Your project",
    //...
    "scripts": {
        "phpunit": [
            "@phpunit-7.2",
            "@phpunit-7.3",
            "@phpunit-7.4"
        ],
        "phpunit-7.2": "docker run -v $PWD:/data quadrixo/phpunit:8-7.2 phpunit -c phpunit.xml",
        "phpunit-7.3": "docker run -v $PWD:/data quadrixo/phpunit:9-7.3 phpunit -c phpunit.xml",
        "phpunit-7.4": "docker run -v $PWD:/data quadrixo/phpunit:9-7.4 phpunit -c phpunit.xml"
    }
}
```

Then run the command :

```sh
$ composer run phpunit
```

# License

This image is licensed under the [CeCILL](https://cecill.info/) license.

[CeCILL 2.1](../LICENSE)
